<?php

use Illuminate\Database\Seeder;
use App\Coupon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $values = ['15'=>'50','12'=>'100','10'=>'200','8'=>'500','5'=>'1000','4'=>'2000','2'=>'5000','1'=>'10000'];

        foreach($values as $key=>$value){
            $coupon = Coupon::create(['value'=>$value, 'can_use'=>$key]);
        }
    }
}
