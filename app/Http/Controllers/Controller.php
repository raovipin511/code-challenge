<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function success($message, $data = null, $statusCode = Response::HTTP_OK) {
        return response()->json([
            'message' => $message,
            'status_code' => $statusCode,
            'data'    => $data
        ], $statusCode);
    }

    function error($message = null, $statusCode, $error = null) {
        return response()->json([
            'message'   => $message,
            'status_code' => $statusCode,
            'error'     => $error
        ], $statusCode);
    }
}
