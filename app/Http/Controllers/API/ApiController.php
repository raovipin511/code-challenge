<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Coupon;

class ApiController extends Controller
{
    //
    public function index(Request $request)
    {
        try {

            $userExist = User::where('email', $request->email)->orwhere('phone', $request->phone)->exists();
            if($userExist){
                return $this->error('Same detail exist', 500, 'Given phone number or email address has been used previously');
            }

            $count = Coupon::where('can_use', '!=',0)->count();
            if($count==0){
                return $this->error('All discount values consumed', 500, 'Discount values have already been given, no more discount values left');
            }

            $discount_value = $this->getValue();
            $createUser = User::create(['name'=> $request->name, 'email'=> $request->email, 'phone'=> $request->phone, 'discount_value'=> $discount_value]);
            if($createUser){
                return $this->success('Discount value', $discount_value);
            }
            
        } catch (Exception $error) {
            return $this->error('Error while form submission', 500, $error->getMessage());
        }
    }

    public function getValue(){
        $coupons  = Coupon::where('can_use', '!=',0)->pluck('can_use','value')->toArray();
        
        # you put each of the values N times, based on N being the probability
        # each occurrence of the number in the array is a chance it will get picked up
        # same is with lotteries
        $numbers = array();
        foreach($coupons as $k=>$v){
            for($i=0; $i<$v; $i++){
                $numbers[] = $k;
            }
        }
        
        # then you just pick a random value from the array
        # the more occurrences, the more chances, and the occurrences are based on "priority"
        $coupon = $numbers[array_rand($numbers)];

        #Less coupon value by 1
        $couponData = Coupon::where('value', $coupon)->first();
        $can_be_used = $couponData->can_use;
        $couponData->can_use = $can_be_used - 1;
        $couponData->save();

        return $coupon;
    }
}
